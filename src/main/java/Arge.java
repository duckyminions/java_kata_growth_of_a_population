public class Arge {

    public static int nbYear(int initialPopulation, double percent, int inhabitants, int targetPopulation) {
        int years = 1;
        int population = (int) (initialPopulation + (initialPopulation * (percent / 100)) + inhabitants);

        while (population < targetPopulation) {
            years++;
            population = (int) (population + (population * (percent / 100)) + inhabitants);
        }
        return years;
    }

    public static int nbYearrecursive(int p0, double percent, int aug, int p) {
        if (p0>=p) return 0;
        else return nbYear((int) (p0+aug+p0*(percent/100)), percent, aug, p) + 1;
    }
}
