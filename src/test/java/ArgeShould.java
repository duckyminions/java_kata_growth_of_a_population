import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;


public class ArgeShould {

    @Test
    public void fixedTest() {
        assertThat(Arge.nbYear(1000, 2, 50, 1200)).isEqualTo(3);
        assertThat(Arge.nbYear(1500, 5, 100, 5000)).isEqualTo(15);
        assertThat(Arge.nbYear(1500000, 2.5, 10000, 2000000)).isEqualTo(10);
    }

    @Test
    public void recursiveMethod() {
        assertThat(Arge.nbYearrecursive(1000, 2, 50, 1200)).isEqualTo(3);
        assertThat(Arge.nbYearrecursive(1500, 5, 100, 5000)).isEqualTo(15);
        assertThat(Arge.nbYearrecursive(1500000, 2.5, 10000, 2000000)).isEqualTo(10);

    }
}